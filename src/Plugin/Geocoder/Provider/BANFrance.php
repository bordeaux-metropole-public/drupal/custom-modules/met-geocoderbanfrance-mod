<?php

namespace Drupal\geocoder_banfrance\Plugin\Geocoder\Provider;

use Drupal\geocoder\ConfigurableProviderUsingHandlerWithAdapterBase;

/**
 * Provides a Addok geocoder provider plugin.
 *
 * @GeocoderProvider(
 *   id = "banfrance",
 *   name = "France BAN (Uses Addok)",
 *   handler = "\Geocoder\Provider\Addok\Addok",
 *   arguments = {
 *     "rootUrl" = "https://api-adresse.data.gouv.fr"
 *   }
 * )
 */
class BANFrance extends ConfigurableProviderUsingHandlerWithAdapterBase {}
